#include "sq.h"
#include "sqPlatformSpecific.h"
#include "mini-printf.h"

FILE *fopen (const char * __filename,
                    const char * __modes) {
  write_serial_string("fopen called!\n");
    return 0; // FIXME: STUB
}

int fflush (FILE *__stream) {
    return 0;
}

int fileno(FILE *__stream) {
  write_serial_string("fileno called!\n");
  return 2;
}

int close(int __fd) {
  return 0;
}

int fclose(FILE *__stream) {
  return 0;
}

int fputc(int ch, FILE *file) {
  return ch;
}

void error(char *s) {
  perror(s);
}

char *strerror(int error) {
  return "Error!";
}

int abs(int x){
  return x > 0 ? x : -x;
}

int vfprintf(FILE *s, const char *format, va_list ap) {
  write_serial_string("vfprintf called!\n");
  return 0;
}

int
printf_pocho(const char *fmt, ...)
{
	int ret;
	va_list va;
	va_start(va, fmt);
	ret = mini_vsnprintf(fmt, va);
	va_end(va);

	return ret;
}

int printf (const char *fmt, ...)
{
	int ret;
	va_list va;
	va_start(va, fmt);
	ret = mini_vsnprintf(fmt, va);
	va_end(va);

	return ret;
}

int vprintf (const char * fmt, va_list va)
{
	int ret;
	ret = mini_vsnprintf(fmt, va);
	return ret;
}

char *getenv(const char *name){
  if (strcmp(name,"FT2_DEBUG") == 0) {
    return "1";
  }
  return "";
}

struct tm *localtime(const time_t *timer) {
  struct tm * ctm = malloc(sizeof(struct tm)) ;
  ctm->tm_sec = 1;
  ctm->tm_min = 0;
  ctm->tm_hour = 0;
  ctm->tm_mday = 20;
  ctm->tm_mon = 1;
  ctm->tm_year = 116;
  ctm->tm_wday = 3;
  ctm->tm_yday = 21;
  ctm->tm_isdst = 0;
  return ctm;
}

char *asctime(const struct tm *timeptr) {
  struct tm ctm;
  return "CURRENT TIME";
}

/* SYSTEM */

void abort(void) {
  write_serial_string("abort called!\n");
} // STUB

//void __end(){ } // STUB
