'From Pharo4.0 of 18 March 2013 [Latest update: #40626] on 17 February 2016 at 8:49:30.338995 pm'!

!SmalltalkImage methodsFor: 'miscellaneous' stamp: 'JanStruz 1/26/2016 18:57'!
openLog
	"This is a _private_ method,
	Because it really belongs to logging facility,
	we should delegate to it at some point "

	^ SerialStream on: nil.! !

!SmalltalkImage methodsFor: 'miscellaneous' stamp: 'JanStruz 1/26/2016 18:57'!
logStdErrorDuring: aBlock
	| stderr |
	[
		"install the line end conversion and force initialize the converter"
		stderr := SerialStream on: nil.
		
		"log in red"
		"stderr nextPut: Character escape; nextPutAll: '[31m'."
		"rund the loggin block"
		aBlock value: stderr.
		"reset the coloring"
		"stderr nextPut: Character escape; nextPutAll: '[0m'."
	] on: Error do: [ :e| "we don't care if the logging to stdout fails..." ].! !


